import UIKit
import AMScrollingNavbar
import OAuthSwift
import SwiftyJSON

class SakeListController: ScrollingNavigationViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var sakes:[JSON] = []
    private let pageSize = 20
    var nowLoading = false
    var selectItem: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        loadSakeList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(tableView, delay: 50.0)
        }
        
        // セルの選択状態を解除する
        if let indexPathForSelectedRow = tableView?.indexPathForSelectedRow {
            tableView.deselectRowAtIndexPath(indexPathForSelectedRow, animated: true)
        }
        
        // キーボードが表示を監視
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(SakeListController.handleKeyboardWillShowNotification(_:)), name: UIKeyboardWillShowNotification, object: nil)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // スクロールした時にキーボードを閉じる
        self.searchBar.resignFirstResponder()
        if (self.nowLoading) {
            return
        }
        
        if (sakes.count % pageSize > 0) {
            return
        }

        if (tableView.dragging) {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

            if (maximumOffset - currentOffset) <= 40 {
                let next_page = (sakes.count / pageSize) + 1
                loadSakeList(next_page, keyword: searchBar.text!)
            }
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return sakes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("sakes")! as UITableViewCell
        cell.textLabel!.text = self.sakes[indexPath.row]["name"].string
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //選択したアイテムを設定
        self.selectItem = self.sakes[indexPath.row]
        performSegueWithIdentifier("toSakeDetailViewController",sender: nil)
    }
    
    // キーボードが表示されるときにキャンセルボタンを有効に
    func handleKeyboardWillShowNotification(notification: NSNotification) {
        searchBar.showsCancelButton = true
    }
    
    // キャンセルボタンが押されたらキャンセルボタンを無効にしてフォーカスをはずす
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let sakeDetailView = segue.destinationViewController as! SakeDetailViewController
        sakeDetailView.currentItem = self.selectItem
        // 遷移前にキーボードを閉じる
        self.searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.loadSakeList(keyword: searchText, search: true)
    }
    
    private func loadSakeList(page: Int = 1, keyword: String = "", search: Bool = false) {
        //AppDelegateのインスタンスを取得
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let parameters :[String:AnyObject] = [
            "page"         : page,
            "per_page"     : pageSize,
            "keyword"      : keyword
        ]
        self.nowLoading = true
        appDelegate.oauthswift.startAuthorizedRequest("http://192.168.56.111:3000/api/v1/sakes",
            method: .GET,
            parameters: parameters,
            success: {
                data, response in
                let dataJson: AnyObject! = try? NSJSONSerialization.JSONObjectWithData(data, options: [])
                if let newSakeList = JSON(dataJson).array {
                    if search {
                        self.sakes = []
                    }
                    self.sakes += newSakeList
                    self.nowLoading = false
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
            , failure: { error in
                if error.code == 401 {
                    // アラートを出す
                    let alert = UIAlertController(title: "セッション切れ", message: "ログインの有効期限が切れました。ログイン画面へ遷移します。", preferredStyle: .Alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) {
                        action in
                        // ログイン画面へ
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let LoginVC = storyboard.instantiateViewControllerWithIdentifier("LoginView")
                        appDelegate.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                        appDelegate.window?.rootViewController = LoginVC
                        appDelegate.window?.makeKeyAndVisible()
                    })
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                print("Error \(error)")
            }
        )
    }
    
    // ナビゲーションバーに検索フィールドを表示する
    private func setupSearchBar() {
        if let navigationBarFrame = navigationController?.navigationBar.bounds {
            let searchBar: UISearchBar = UISearchBar(frame: navigationBarFrame)
            searchBar.delegate = self
            searchBar.placeholder = "Search"
            searchBar.showsCancelButton = false
            searchBar.autocapitalizationType = UITextAutocapitalizationType.None
            searchBar.keyboardType = UIKeyboardType.Default
            navigationItem.titleView = searchBar
            navigationItem.titleView?.frame = searchBar.frame
            self.searchBar = searchBar
        }
    }
}

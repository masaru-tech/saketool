import UIKit
import SwiftyJSON

class SakeDetailViewController: UIViewController{
    var searchBar: UISearchBar!
    @IBOutlet weak var counter: UILabel!
    var currentItem: JSON?
    @IBOutlet weak var sakeName: UILabel!
    let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBAction func dislikeClicked(sender: AnyObject) {
        let count:Int? = Int(counter.text!)
        let sum:Int? = count! - 1
        counter.text = String(sum!)

        appDelegate.oauthswift.startAuthorizedRequest("http://192.168.56.111:3000/api/v1/sakes/\(self.currentItem!["id"].intValue)/dislike",
            method: .PATCH,
            parameters: [:],
            success: { data, response in
                print("success")
            }
            , failure: { error in
                print("Error \(error)")
            }
        )
    }

    @IBAction func likeClicked(sender: AnyObject) {
        let count:Int? = Int(counter.text!)
        let sum:Int? = count! + 1
        counter.text = String(sum!)

        appDelegate.oauthswift.startAuthorizedRequest("http://192.168.56.111:3000/api/v1/sakes/\(self.currentItem!["id"].intValue)/like",
            method: .PATCH,
            parameters: [:],
            success: { data, response in
                print("success")
            }
            , failure: { error in
                print("Error \(error)")
            }
        )
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        sakeName.text = self.currentItem!["name"].string
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

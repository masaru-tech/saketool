import UIKit
import OAuthSwift

class LoginViewController: UIViewController{
    
    @IBAction func signupWithOAuth(sender: AnyObject) {
        //AppDelegateのインスタンスを取得
        let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.oauthswift.authorize_url_handler = SafariURLHandler(viewController: self)
        
        appDelegate.oauthswift.authorizeWithCallbackURL(
            NSURL(string: "integration-sample-ios://oauth-callback/ios")!,
            scope: "", state: "MyAPI",
            success: { credential, response, parameters in
                print(credential.oauth_token)
                
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setValue(credential.oauth_token, forKey: "oauth_token")
                defaults.setValue(credential.oauth_refresh_token, forKey: "refresh_token")
                defaults.setValue(credential.oauth_token_expires_at, forKey: "expires_at")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let sakeListVC = storyboard.instantiateViewControllerWithIdentifier("FirstView")
//                UIApplication.sharedApplication().keyWindow?.rootViewController = sakeListVC
                
                sakeListVC.modalTransitionStyle = .CrossDissolve
                self.presentViewController(sakeListVC, animated: true, completion: nil)
            },
            failure: { error in
                print(error.localizedDescription)
            }
        )
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

